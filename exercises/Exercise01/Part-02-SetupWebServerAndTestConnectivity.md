# Exercise #1 : Part 2
## Setup web server and test connectivity

### Intro

In this section we will verify HTTP connectivity from your local machine to a services running on your Lab instance.  For this exercise you will need connectivity to your lab instance via SSH and a web browser on your local machine.  Also make sure you have your hostname or IP available as we will use it to verify the connectivity.


### Install web server

We will be using `lighttpd` as a simple web service in this exercise. We are going to go ahead and install it from a package and manage the service with systemd.  The first think we need to do is make sure our Ubuntu package database is current to the Ubuntu provided package repositories.  We can do that with the `update` option for `apt`.

~~~shell
sudo apt update
~~~

Now we can install the `lighttpd` package.

~~~shell
sudo apt install -y lighttpd
~~~

#### Verify that the `lighttpd.service` unit is enabled and started

Using systemd we can make sure that the `lighttpd.service` unit is enabled.

~~~shell
sudo systemctl enable lighttpd.service
~~~

We also can make sure that the `lighttpd.service` unit is started.

~~~shell
sudo systemctl start lighttpd.service
~~~

Using systemd we can verify the status of the `lighttpd` service.

~~~shell
sudo systemctl status lighttpd.service
~~~

You should see that the `lighttpd.service` is "active (running)". You may also need to press `q` to exit the pager showing the status if you prompt is not returned to the shell.

### Put some content on a webpage

In this section we will demonstrate how we can use an editor to create and modify files on the Lab instance.  Here we will be creating an `index.html` file that should be returned when the site is queried.

In this exercise you can use which ever file editor you are comfortable with.  Experienced users tend to use `vim` while `nano` is a simple option for individuals with less comfort or experience on the the Linux shell. Continue with one of two options below.

###### 1. Using `vim`

~~~shell
sudo vim /var/www/html/index.html
~~~

###### 2. Using `nano`

~~~shell
sudo nano /var/www/html/index.html
~~~

#### Add content to `/var/www/html/index.html`

That you are in the editor populate the new file with this content.  Replace the `NAME` part with your name to demonstrate that you are connected to the correct host.

~~~html
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <title>Welcome to a service on your Lab instance</title>
</head>
<body>
        Hello World!
        <br>
        NAME, Your connection works
</body>
</html>
~~~

### Test the connection locally

Now that you have a website and a web server serving that page let's verify it first from your lab instance using `curl`. The `curl` command will print the raw html from the page to standard out on your shell.

~~~shell
curl http://localhost
~~~

You should see the content of your `index.html` file printed to the shell.

### Test connectivity from your desktop

In this section let's verify that we can load the webpage from your desktop.  This will verify that there is not a firewall that is going to cause us issues later in the labs.

From your browser of chose got to this website, replace `lab42.agilebrainslabs.com` with hostname or IP provided to you for your lab instance.

`http://lab42.agilebrainslabs.com`

If everything is correct you should see the simple rendered webpage in your browser.

### Make your own changes

The labs will build on previous labs.  Now it is time to take what we have already demonstrated and modify the content of the website to include your Lab Instances hostname instead of your name.  Once you have made that change verify it within your browser of choice.

### Clean up

It's time to clean up after ourselves.  We do not want `lighttpd` listening on port 80 on our host in upcoming labs.  So let us fix that.

First we will stop the `lighttpd` process.

~~~shell
sudo systemctl stop lighttpd.service
~~~

Next let's disable the service.

~~~shell
sudo systemctl disable lighttpd.service
~~~

Now we can verify that the service is no longer in a "running" state.

~~~shell
sudo systemctl status lighttpd.service
~~~

As before you may need to press `q` to exit the pager if you are not returned to the shell prompt.

Now it is time to remove the `lighttpd` package from the host

~~~shell
sudo apt remove -y lighttpd
~~~

###

This concludes part 2 of Hands-on Exercise #1.  Continue on to Part 3 next.

[Part 3: Download lab content](Part-03-DownloadLabContent.md)
