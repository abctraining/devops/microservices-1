# Exercise #7 : Part 3
## K8s Basic Logging

We will use the 'yaml' files from the repos `src/Exercise07` directory for the following exercises.

Run a pod and check logs

```bash
kubectl create -f basic-logging.yaml
```

Check logs

```bash
kubectl logs pod/counter
```

Follow the logs from the pod

```bash
kubectl logs -f pod/counter
```

'CTRL+C' to exit

Destroy all resources

```bash
kubectl delete -f basic-logging.yaml
```

This concludes part 3 of Hands-on Exercise #7.  Continue on to Part 4 next.

[Part 4: Monitor Resources with Kubernetes](Part-04-Monitoring.md)