# Exercise #7 : Part 4
## K8s Monitoring with Prometheus + cAdvisor DaemonSet + Grafana

Run cAdvisor Daemonset, prometheus, grafana in monitoring namespace

```bash
kubectl create -f cadvisor-prom-grafana.yaml
```

Check deployed resources

```bash
kubectl get all -o wide -n monitoring
```

Get the NodePort of the service `prometheus`, and `grafana`.
In the below example, the nodeport for grafana:31327, prometheus:31804

###### Dashboards
* Open TCP port 31327 and 31804 on any node in the cluster.
* Access the Prometheus UI `http://<NODE IP>:31804`
* Access the Grafana UI `http://<NODE IP>:31327`
  * username: _admin_
  * password: _admin_
* Select "Kubernetes Pod Resources" in Grafana
* try selecting some metrics, check the logs and create graphs

###### Destroy all resources

```bash
kubectl delete -f cadvisor-prom-grafana.yaml
```

This concludes part 4 of Hands-on Exercise #7.  Continue on to Part 5 next.

[Part 5: Simplify Application management with Helm](Part-05-Helm.md)