# Exercise #7 : Part 1
## Creating ConfigMap


###### Create a Configmap and check log level

In this lab we will use the 'yaml' files from the repos `src/Exercise07` directory.

Create a deployment which does not refer to config map ( has log level hard coded as error)

```bash
kubectl create -f reader-deployment.yaml
```

Check if you have any ConfigMaps

```bash
kubectl get configmaps -o wide
```

Check for the running pods and describe the pod, and check logs for the running pod

```bash
kubectl get po,deploy -o wide

kubectl describe <pod_name>

kubectl logs <pod_name>
```

---

Create a deployment which uses to config map

Before the deployment define a ConfigMap as a key-value pair

```bash
kubectl create configmap logger --from-literal=log_level=debug
```

Now create the Deployment

```bash
kubectl create -f reader-configmap-deployment.yaml
```

Check if the Config Map was created

```bash
kubectl get configmaps -o wide
```

Check for the running pods and describe the pod, and check logs for the running pod

```bash
kubectl get po,deploy -o wide

kubectl describe <pod_name>

kubectl logs <pod_name>

kubectl delete --all deploy --namespace=default
```

This concludes part 1 of Hands-on Exercise #7.  Continue on to Part 2 next.