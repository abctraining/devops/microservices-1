# Microservice 1: Essentials
# Hands-on Exercise #7

### Objective

Work through more advanced Kubernetes resources. Including `ConfigMaps`, `Jobs`, `Logging`, `Monitoring`, and `Helm`

### Parts

[Part 1: Add environmental configs to Pods with ConfigMaps](Part-01-ConfigMaps.md)

[Part 2: Schedule Pods for admin tasks with Jobs](Part-02-Jobs.md)

[Part 3: Explore Logging from Pods](Part-03-Logging.md)

[Part 4: Monitor Resources with Kubernetes](Part-04-Monitoring.md)

[Part 5: Simplify Application management with Helm](Part-05-Helm.md)
