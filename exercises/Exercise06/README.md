# Microservice 1: Essentials
# Hands-on Exercise #6

### Objective

Learn the Basics of Kubernetes.  Work through resource organization with `namespaces` and `labels`.  Learn resource control and access with `deployments`, `serices`, and `autoscaling`.

### Parts

[Part 1: Organize Kubernetes resources with Namespaces](Part-01-Namespaces.md)

[Part 2: Group Kubernetes resources with Labels](Part-02-Labels.md)

[Part 3: Deploy Kubernetes Pods with Deployments](Part-03-Deployments.md)

[Part 4: Expose Kubernetes Pods with Services](Part-04-Services.md)

[Part 5: Scale Kubernetes Pods with Autoscaling](Part-05-Autoscaling.md)