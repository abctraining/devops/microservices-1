# Exercise #6 : Part 4
## Practise creating Pods, Deployments and Services

Create a pod, deployment and service using the Imperative method

Create a deployment nginx

```bash
kubectl create deployment nginx-deployment  --image nginx --port 80
```

Check the currently running pods, deployments, services and replica sets

```bash
kubectl get po,deploy,svc,rs -o wide
```

Expose the Deployment as a Service of Loadbalancer

```bash
kubectl expose deploy/nginx-deployment --type=LoadBalancer --name nginx-service
```

Check the currently running pods, deployments, services and replica sets

```bash
kubectl get po,deploy,svc,rs -o wide
```

_Check for the nginx application from url , using the IP_

Do a describe on the nginx service , using the IP try doing a curl for that port

```bash
kubectl describe svc nginx-service
```

verify by: `curl <ip_address>:<port_number>`

Other ways to verify

```bash
kubectl proxy &
curl http://localhost:8001/api/v1/namespaces/default/services/nginx-service/proxy/
curl http://$(kubectl get svc nginx-service --template='{{.spec.clusterIP}}'):80
```
---

The Needed Files are located in the local repos `src/Exercise06/` folder

Create a Service from a Pod

```bash
kubectl create -f sample_pod.yaml
```

Use `exec` and `logs` commands

```bash
IP=`kubectl describe pod/nginx-apparmor | grep -i "^IP:" | awk -F" " '{print $2}'`
curl $IP:80
kubectl logs pod/nginx-apparmor
```

You will see the logs

```bash
kubectl exec -it pod/nginx-apparmor -- /bin/bash
```

This will take you to the container, if pod has multiple containers, use -c option for container name.

Use `exit` to leave of the container.

Check the currently running pods

```bash
kubectl get po -o wide
```

Expose the pod as a service

```bash
kubectl expose pod nginx-apparmor --type NodePort --name nginx-apparmor-service
```

check running services again

```bash
 kubectl get po,deploy,svc,rs -o wide
```

###### verify

```bash
kubectl describe svc nginx-apparmor-service
```

verify by
```bash
curl <ip_address>:<port_number>
```

### Clean up

```bash
kubectl delete svc nginx-service
kubectl delete deploy nginx-deployment
kubectl delete svc nginx-apparmor-service
kubectl delete pod nginx-apparmor
```

To stop the proxy we must first bring the job to the foreground.

```bash
jobs
fg
```

Use `CTRL+C` to stop the proxy while it is in the foreground.

This concludes part 4 of Hands-on Exercise #6.  Continue on to Part 5 next.

[Part 5: Scale Kubernetes Pods with Autoscaling](Part-05-Autoscaling.md)