# Exercise #2 : Part 2
## Docker Hello World

### Intro

In this section we will run our first Docker commands and start some basic docker containers.

### Run Docker Hello world


##### Pull the image

Before we start the container based on the `helloworld` image let us first `pull` that image to our local host image store.

~~~shell
docker pull hello-world
~~~

You will notice that it pulled down the hello-world image from Docker Hub.

You can even verify it exists with the `docker image ls` command.

~~~shell
docker image ls
~~~

We will talk more about that output later.  For now we see that the image exists locally on our lab instance.


##### Run the image

Now let us actually run the `hello-world` image as a container.

~~~shell
docker run hello-world
~~~

The `hello-world` image dumps a message to the shell and exits.  Notice the output from the container.

#### Something more ambitious

The output from `hello-world` indicated something more ambitious.  Go ahead and try it and explore a bit and see what you find.

How can you tell if you are on a shell inside a container?

### Conclusion

You have run your first container congratulations.  We will build on this as we move further.

This concludes part 2 of Hands-on Exercise #2.  Continue on to Part 3 next.

[Part 3: Docker command basics](Part-03-DockerCommandBasics.md)
