# Exercise #5 : Part 1
## Single Node Cluster using K3s

Before installing a K3s Kubernetes cluster let's make sure the local copy of the application repositories are up-to-date.

_update local package repo_
```bash
sudo apt update
```

There are a few packages required to continue with the K3s installation.  Let's install them.

_add additional packages_
```bash
sudo apt install -y bash-completion software-properties-common
```

Now the system is prepped for the K3s install.  The actuall install is done using a script that can be downloaded from 'https://get.k3s.io' and run with root privileges. We also will set the 'K3S_KUBECONFIG_MODE' to '644' to allow local users on the system to read the credentials for the K3s admin user.

_Install K3s_
```bash
curl -sfL https://get.k3s.io | sudo INSTALL_K3S_VERSION=v1.22.12+k3s1 K3S_KUBECONFIG_MODE="644" INSTALL_K3S_EXEC="--disable=servicelb" sh -
```

After a short time we should be able to verify the cluster is running by displaying the K3s cluster 'nodes'.  The status should be 'Ready' when the K3s cluster is started.

_Verify the K3s Node_
```bash
sudo k3s kubectl get nodes
```

###### Kubectl

The K3s installation not only installed K3s but also added a `kubectl` client to `/usr/local/bin`.

_Verify kubectl binary_
```bash
kubectl version
```

The `kubectl` client is in place but it also needs to be configured.  It needs to know where to connect to the Kubernetes API endpoint and with what user and credentials.  The K3s installation script created a `admin` user with credentials stored in `/etc/rancher/k3s/k3s.yaml`.  If we copy those credentials to the location that `kubectl` expects them, `kubectl` will be able to connect to the new K3s cluster.   

_Configure kubectl_
```bash
mkdir -p ~/.kube
cat /etc/rancher/k3s/k3s.yaml > ~/.kube/config
chmod 600 ~/.kube/config
```

Let's now verify that our local user can connect to the K3s cluster using `kubectl` and display the `nodes` for the cluster.

_Verify kubectl connectivity_
```bash
kubectl get nodes
```

If the the cluster `nodes` are displayed we can see that the K3s cluster is up and running.

This concludes part 1 of Hands-on Exercise #5.  Continue on to Part 2 next.

[Part 2: Add MetalLB to the Kubernetes Cluster](Part-02-MetalLB.md)
