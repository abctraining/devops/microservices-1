# Microservice 1: Essentials
# Hands-on Exercise #5

### Objective

Install a simple single node Kubernetes cluster using the `K3s` distribution.  Add to the cluster `MetalLB` to provide the Loadbalancer functionality for the Cluster.

### Parts

[Part 1: Install a Single Node Kubernetes Cluster](Part-01-K3sSingleNodeCluster.md)

[Part 2: Add MetalLB to the Kubernetes Cluster](Part-02-MetalLB.md)

[Part 3: Add the Kubernetes Cluster to Rancher](Part-03-Rancher.md)
